# GenA - feature selection via genetic algorithm

### Description
This is implementation of feature selection using genetic algorithm.

### Installation
`pip install genA-selector`

### Usage
[Example of usage](example.ipynb)

### Authors and acknowledgment
[**Article**](https://www.neuraldesigner.com/blog/genetic_algorithms_for_feature_selection)
